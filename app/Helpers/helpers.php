<?php

use Carbon\Carbon;

function calcDaysDifference($from , $to)
{
    $from = Carbon::parse($from);
    return $from->diffInDays($to);
}