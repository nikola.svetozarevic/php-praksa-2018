<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\EntryNotFoundException;
use App\Exceptions\NotPendingException;
use App\Http\Controllers\Controller;
use App\Mail\ResponseMail;
use App\Repositories\RequestRepository;
use Illuminate\Http\Request;
use \App\Models\Request as RequestModel;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    /**
     * @var RequestRepository
     */
    private $requestRepository;
    public function __construct(RequestRepository $requestRepository) {
        $this->requestRepository = $requestRepository;
    }

    public function index()
    {
        $requests = $this->requestRepository->paginate(10, null,  RequestModel::REQ_STAT_PEN);
        return view('requests.index', compact('requests'));
    }

    public function update(Request $request)
    {
        $data['explanation'] = $request->explanation;
        if (!empty($request->accept))
        {
            $data['status'] = RequestModel::REQ_STAT_ACC;
        }else
        {
            $data['status'] = RequestModel::REQ_STAT_REJ;
        }

        try {
            $request = $this->requestRepository->update($data, $request->request_id, true);
        } catch (EntryNotFoundException $e) {
            abort(404);
        } catch (NotPendingException $e) {
            abort(404);
        }
        Mail::to($request->user->email)->queue(new ResponseMail($request));

        return redirect()->route('request.index');
    }
}
