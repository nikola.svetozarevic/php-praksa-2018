<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\EntryNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Repositories\TeamRepository;
use Illuminate\Http\Request;
use App\Http\Requests\TeamRequest;

class TeamController extends Controller
{
    /**
     * @var teamRepository
     */
    private $teamRepository;
    public function __construct(TeamRepository $teamRepository) {
        $this->teamRepository = $teamRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $teams = $this->teamRepository->paginate(4);
       return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $data['teamName'] = $request->teamName;
        $this->teamRepository->create($data);
        return redirect()->route('team.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $team = $this->teamRepository->getById($id, true);
        } catch (EntryNotFoundException $e) {

            abort(404);
        }
        return view('teams.show', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $team = $this->teamRepository->getById($id);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return view('teams.edit', compact('team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, $id)
    {
        $data['teamName'] = $request->teamName;
        try {
            $this->teamRepository->update($data, $id);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return redirect()->route('team.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->teamRepository->delete($id);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return redirect()->route('team.index');
    }
}
