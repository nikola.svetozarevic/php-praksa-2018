<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\EntryNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * @var UserRepository
     * @var teamRepository
     */
    private $userRepository;
    private $teamRepository;
    public function __construct(UserRepository $userRepository, TeamRepository $teamRepository) {
        $this->userRepository = $userRepository;
        $this->teamRepository = $teamRepository;
    }

    /**
     * Display a listing of the resource.
     *
     */
    //PAGINATED -- include render in template
    public function index(Request $request)
    {
        $search = $request->search;
        $teamsChecked = $request->teams;
        $fromPicked = $request->fromPicked;
        $toPicked = $request->toPicked;
        if (!empty($fromPicked)) {
            $fromPicked = Carbon::parse($fromPicked)->format('Y-m-d');
        }
        if (!empty($toPicked)) {
            $toPicked = Carbon::parse($toPicked)->format('Y-m-d');
        }
        $users = $this->userRepository->paginate(10, $search, $teamsChecked, $fromPicked, $toPicked);
        $teams = $this->teamRepository->all();
        if (empty($teamsChecked)) {
            $teamsChecked = [];
        }
        $request =  $users->appends(Input::except('page'));
        return view('users.index', compact('users','teams', 'search', 'teamsChecked', 'fromPicked', 'toPicked', 'page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $teamPreChecked = null)
    {
            $teamsChecked = $request->teams;
            $teamPreChecked = $request->teamPreChecked;
            $teams = $this->teamRepository->all();
            if (empty($teamsChecked)) {
                $teamsChecked = [];
            }
            return view('users.create', compact('teams', 'teamsChecked', 'teamPreChecked' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->only(['firstName', 'lastName', 'password', 'email', 'gender', 'earnedDaysOff']);
        $data['password'] = Hash::make($request->password);
        $this->userRepository->create($data, $request->teams);
        if (!empty($request->teamPreChecked))
        {
            return redirect()->route('team.show', $request->teamPreChecked);
        }
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->userRepository->getById($id, true);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        if ($user->role === User::ROLE_ADMIN)
        {
            $user->role = "admin";
        } else
        {
            $user->role = "user";
        }
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $teams = $this->teamRepository->all();
        try {
            $user = $this->userRepository->getById($id);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return view('users.edit', compact('user', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
       //Retrieving A Portion Of The Input Data
        $data = $request->only(['firstName', 'lastName','email', 'gender','earnedDaysOff']);
        if (!empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }
        try {
            $this->userRepository->update($data, $id, $request->teams);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->userRepository->delete($id);
        } catch (EntryNotFoundException $e) {
            abort(404);
        }
        return redirect()->route('user.index');
    }
}
