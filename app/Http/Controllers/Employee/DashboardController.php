<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Http\Requests\DaysOffRequest;
use App\Mail\RequestMail;
use App\Repositories\RequestRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use \App\Models\Request as RequestModel;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    /**
     * @var RequestRepository
     */
    private $requestRepository;
    public function __construct(RequestRepository $requestRepository) {
        $this->requestRepository = $requestRepository;
    }

    //methods: index, create, store
    //INDEX
    public function index ()
    {
        $id = Auth::user()->id;
        $requests = $this->requestRepository->paginate(2,  $id,  RequestModel::REQ_STAT_PEN);
        return view('dashboard.index', compact('requests', 'page'));
    }
    //CREATE
    public function create(Request $request)
    {
        return view('dashboard.create' );
    }
    //STORE
    public function store(DaysOffRequest $request)
    {
        $data['note'] = '';
        $data = $request->only(['from', 'to', 'note', 'requestedDaysOff','user_id']);
        if (!empty($data['from'])) {
            $data['from'] = Carbon::parse($data['from'])->format('Y-m-d');
        }
        if (!empty($data['to'])) {
            $data['to'] = Carbon::parse($data['to'])->format('Y-m-d');
        }
        $data['requestedDaysOff'] =  calcDaysDifference($data['from'], $data['to']);
        $data['user_id'] = Auth::user()->id;

        $request = $this->requestRepository->create($data);
        Mail::to('spasicjovica@hotmail.com')->queue(new RequestMail($request));

        return redirect()->route('dashboard.index');
    }
}
