<?php

namespace App\Http\Controllers\Employee;

use App\Exceptions\EntryNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\MyInfoRequest;
use App\Repositories\TeamRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class MyInfoController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }
    //methods needed in MyInfoController: index, edit, update
    //INDEX method is like show
   public function index ()
   {
       $user = Auth::user();
       return view('myinfo.index', compact('user'));
   }
   //EDIT
    public function edit()
    {
        $user = Auth::user();
        return view('myinfo.edit', compact('user'));
    }
    //UPDATE
    public function update(MyInfoRequest $request)
    {
        $id = Auth::user()->id;
        //Retrieving A Portion Of The Input Data
        $data = $request->only(['firstName', 'lastName','email', 'gender']);
        $this->userRepository->update($data, $id);
        return redirect()->route('myinfo.index' );
    }
}
