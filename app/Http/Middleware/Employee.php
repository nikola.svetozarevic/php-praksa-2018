<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Employee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! empty(Auth::user())) {
            if (Auth::user()->role == User::ROLE_EMPLOYEE) {
                return $next($request);
            } else {
                return redirect()->route('home');
            }
        }

        return redirect()->route('login');
    }
}
