<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    const REQ_STAT_ACC = 'accepted';
    const REQ_STAT_PEN = 'pending';
    const REQ_STAT_REJ = 'rejected';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from', 'to', 'status', 'note','requestedDaysOff', 'user_id', 'explanation'
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Scope a query to only include accepted requests.
     * @param $query
     * @return mixed
     */
    public function scopeAccepted($query)
    {
        return $query->where('status', Request::REQ_STAT_ACC);
    }

    /**
     * Scope a query to only include pending requests.
     * @param $query
     * @return mixed
     */
    public function scopeByStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * Scope a query to only include request in the future.
     * @param $query
     * @return mixed
     */
    public function scopeAfterToday($query)
    {
        $todayDate = Carbon::now()->format('Y-m-d');
        return $query->whereDate('from', '>', $todayDate);
    }

    public function getFromAttribute($value)
    {
         return Carbon::parse($value)->format('d.m.Y.');
    }

    public function getToAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y.');
    }

    public function getDateRange()
    {
      return '(' . $this->from . ' - ' . $this->to . ")";
    }
}
