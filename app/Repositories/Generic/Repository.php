<?php
namespace App\Repositories\Generic;


use App\Exceptions\EntryNotFoundException;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var
     */
    protected $model;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }
    /**
     * Specify Model class name
     * @return mixed
     */
    abstract function model();
    ///////////////////////////////////////////////////////
    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id)
    {
        $instance = $this->getById($id);

        $instance->update($data);

        return $instance;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $instance = $this->getById($id);
        $instance->delete();
        return true;
    }

    /**
     * @param $id
     * @param array $columns
     * @throws EntryNotFoundException
     * @return mixed
     */
    public function getById ($id, $columns = array('*'))
    {
        $response = $this->model->find($id, $columns);

        if (empty($response)) {
            throw new EntryNotFoundException();
        }
        return $response;
    }

    public function makeModel()
    {
        $model = $this->app->make($this->model());
        if (!$model instanceof Model)
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model->newQuery();
    }
}