<?php

namespace App\Repositories\Generic;


interface RepositoryInterface {
//1.Get all records for the concrete entity
//2.Get paginated set of records
//3.Create a new record
//4.Get record by it’s primary key
//6.Update a record
//7.Delete a record

    public function all($columns = array('*'));
    public function paginate($perPage, $columns = array('*'));
    public function create(array $data);
    public function getById($id, $columns = array('*'));
    public function update(array $data, $id);
    public function delete($id);
}
