<?php
namespace App\Repositories;


use App\Exceptions\EntryNotFoundException;
use App\Models\Team;
use App\Repositories\Generic\Repository;

class TeamRepository extends Repository
{
    function model()
        {
            return 'App\Models\Team';
        }
    //PAGINATE
    public function paginate($perPage, $columns = array('*'))
    {
        $teams = Team::withCount('users');
        $teams = $teams->orderBy('id')->paginate($perPage, $columns = array('*'));
        return $teams;
    }
    //GETBYID
    public function getById ($id, $includeUsers = false, $columns = array('*'))
    {
        $team = Team::where('id', $id)->withCount('users');
        if (!empty($includeUsers)) {
            $team = $team->with('users');
        }
        $team = $team->first();
        if (empty($team)) {
            throw new EntryNotFoundException();
        }
        return $team;
    }
    //DELETE
    public function delete($id)
    {
        $team = $this->getById($id);
        $team->users()->detach();
        $team->delete();
        return true;
    }
}