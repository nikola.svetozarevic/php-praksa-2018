<?php

namespace App\Repositories;


use App\Exceptions\EntryNotFoundException;
use App\Models\User;
use App\Repositories\Generic\Repository;
//use App\Repositories\UserRepository;

class UserRepository extends Repository
{
    /**
     * Specify Model class name
     * @return mixed
     */
    function model()
    {
        return 'App\Models\User';
    }
   public function paginate($perPage, $search = null, $teams=null, $fromPicked=null, $toPicked=null, $columns = array('*'))
    {
       $users = User::with(['requests' => function($query){
            $query->accepted()->AfterToday();
        }, 'teams'
        ]);

        if (!empty($search)) {
            $users = $users->where(function ($query) use ($search) {
                $query->where( 'firstName', 'LIKE', '%' . $search . '%' )
                    ->orWhere ( 'lastName', 'LIKE', '%' . $search . '%' )
                    ->orWhere ( 'email', 'LIKE', '%' . $search . '%' );
            });
        }

        if (!empty($teams)) {
            $users = $users->whereHas( 'teams', function($query) use ($teams) {
                $query->whereIn('id', $teams);
            });
        }
       if (!empty($fromPicked) && !empty($toPicked )) {
           $users = $users->whereHas('requests', function ($query) use ($fromPicked, $toPicked) {
           $query->whereBetween('from', [$fromPicked, $toPicked])
                 ->accepted();
           });
       }

        $users = $users->orderBy('id')->paginate($perPage, $columns = array('*'));

        return $users;
    }
    //CREATE
    public function create(array $data, $teamIds = null)
    {
       $user = parent::create($data);

       $user->teams()->sync($teamIds);

        return $user;
    }
    //UPDATE
    public function update(array $data, $id, $teamIds = null)
    {
        $user = parent::update($data, $id);

        if (!empty($teamIds))
        {
            $user->teams()->sync($teamIds);
        }
        return $user;
    }
    //GETBYID
    public function getById ($id, $includeTeams = false, $columns = array('*'))
    {
        $user = User::where('id', $id);
        if (!empty($includeTeams)) {
            $user = $user->with('teams');
        }
        $user = $user->first();
        if (empty($user)) {
            throw new EntryNotFoundException();
        }
        return $user;
    }
    //DELETE
    public function delete($id)
    {
        $user = $this->getById($id);
        $user->teams()->detach();
        $user->delete();
        return true;
    }

    //TEST
    public function test()
    {
        //Neka dodatna nasa metoda; Takodje mozemo da prepisemo i postojece iz repository-ja i da ih izmenimo.
    }

}