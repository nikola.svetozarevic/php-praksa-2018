<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Request::class, function (Faker $faker) {
    return [
        'from' => $faker->dateTimeThisYear($max = '2018-12-31 23:00:00', $timezone = 'Europe/Belgrade'),
        'to' => function (array $request) use ($faker) {
            $date = Carbon::parse($request['from']->format('d-m-Y'));
            $date->addDays($faker->numberBetween(1, 20));

            return $date;
        },
        'status' => $faker->randomElement($array = ['accepted', 'rejected', 'pending']),
        'note' => str_random(20),
        'explanation' => str_random(50),
        'requestedDaysOff' => rand(1, 20),
        'user_id' => rand(1, 10),
    ];
});
