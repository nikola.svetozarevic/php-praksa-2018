<?php

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = ['JS', 'PHP', 'ASP.NET', 'JAVA', 'Unity'];
        foreach ($teams as $team) {
            factory(Team::class)->create([
                'teamName' => $team,
            ]);
        }
    }
}
