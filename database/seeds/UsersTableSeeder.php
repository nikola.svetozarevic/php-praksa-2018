<?php

use App\Models\Team;
use App\Models\User;
use App\Models\Request;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'firstName' =>'ADMIN',
            'lastName' => 'AdminLastName',
            'email' => 'admin@example.com',
            'password' => Hash::make('admin'),
            'role' => User::ROLE_ADMIN,
        ]);

        $teams = Team::all();
        factory(User::class, 20)->create()->each(function ($user) use ($teams) {
            $currentTeams = $teams->random(rand(1, 5));

            $user->requests()->saveMany(factory(Request::class, 4)->make());
            $user->teams()->saveMany($currentTeams);
        });
    }
}
