@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create new Holiday Request.  ') }}{{ ' Available number of days: ' }}{{ Auth::user()->earnedDaysOff}}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('dashboard.store') }}" aria-label="{{ __('Create new Holiday Request') }}">
                            @csrf
                            {{--DATEPICKER jQuery--}}
                            <br><h5>Select a Date Range for request</h5><br>
                            <label for="from">From</label>
                                <input type="text" id="from" name="from" class="form-control{{ $errors->has('from') ? ' is-invalid' : '' }}" value="{{ old('from') }}">
                                    @if ($errors->has('from'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('from') }}</strong>
                                        </span>
                                    @endif

                            <label for="to">to</label>
                                <input type="text" id="to" name="to" class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" value="{{ old('to') }}">
                                    @if ($errors->has('to'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('to') }}</strong>
                                        </span>
                                    @endif

                            <br><br><h5>Insert a note</h5>
                                <textarea id="note" name="note" class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}">{{ Request::old('note') }}</textarea>

                                    @if ($errors->has('note'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('note') }}</strong>
                                            </span>
                                    @endif

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection







