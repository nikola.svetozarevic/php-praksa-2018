@extends('layouts.app')
@section('content')
    <h4>My Requests:</h4>
    <div class="col-xs-12 col-sm-2 add_new">
        <a href="{{ route('dashboard.create') }}" class="btn btn-secondary">Create New Request</a>
    </div><br>

    <div class="container">
        <div id="wrapper">
            <div id="firstList"><h4>Pending</h4>
                <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">From</th>
                                <th scope="col">To</th>
                                <th scope="col">No. of R.days</th>
                                <th scope="col">Note</th>
                                <th scope="col">user_id</th>
                                <th scope="col">STATUS</th>
                            </tr>
                            </thead>
                            <tbody>
                                    @php($rowOrder = "table-secondary")
                                    @foreach($requests as $request)
                                    @php($rowOrder = ($rowOrder === "table-secondary") ? 'table-primary' : 'table-secondary')
                                    <tr class="{{$rowOrder}}">
                                        <th scope="row">{{ $request->id }}</th>
                                        <td>{{ $request->from }}</></td>
                                        <td>{{ $request->to }}</td>
                                        <td>{{ $request->requestedDaysOff }}</td>
                                        <td>{{ $request->note }}</td>
                                        <td>{{ $request->user_id }}</td>
                                        <td>{{ $request->status }}</td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        </div>
                        </div>


                        <div id="secondList"><h4>History</h4>
                        </div>

                        </div>
                        </div>


{{--LARAVEL NEXT I PREVIOWS --}}
{{ $requests->render() }}
@endsection
