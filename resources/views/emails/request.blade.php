<html>
<body>
Dear  Sir or Madam, <br>
I am sending You a holiday request from {{$request->from }} to {{ $request->to }} .
@if (!empty($request->note))
    <p>
        {{ $request->note }}
    </p>
@endif
</body>
</html>