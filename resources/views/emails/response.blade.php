<html>
<body>
    Hi Employee, <br>
    Your request from {{ $request->from }} to {{ $request->to }} has been {{ $request->status }}.
    @if (!empty($request->explanation))
        <p>
            {{ $request->explanation }}
        </p>
    @endif
</body>
</html>