<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{--Laravel JS Scripts--}}
    <script src="{{ asset('js/app.js') }}" ></script>
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    {{--Template JS Scripts--}}
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/bootadmin.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    {{-- DATEPICKER SCRIPT- jQuery --}}
    {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--}}
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            var dateFormat = "mm/dd/yy",
            // var dateFormat = "mm/dd/yy",
                from = $( "#from" )
                    .datepicker({
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 2,
                        dateFormat: "mm/dd/yy"
                    })
                    .on( "change", function() {
                        to.datepicker( "option", "minDate", getDate( this ) );
                    }),
                to = $( "#to" ).datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 2,
                    dateFormat: "mm/dd/yy"
                })
                    .on( "change", function() {
                        from.datepicker( "option", "maxDate", getDate( this ) );
                    });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }

                return date;
            }
        } );
    </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- STYLES -->
    <!-- LARAVEL Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <!-- Template Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootadmin.min.css') }}" rel="stylesheet">

    {{--DATEPICKER CSS jQuery--}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}

    <link href="{{ asset('css/my_css.css') }}" rel="stylesheet">
</head>
<!-- TEMPLATE and Laravel Body MIX -->
<body class="bg-light">
{{--Upward navbar--}}
<nav class="navbar navbar-expand navbar-dark bg-primary">
    <a class="sidebar-toggle mr-3" href="#"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand" href="{{ route('home') }}">TimeOFF Management</a>

    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <!-- Laravel Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-user"></i> {{ Auth::user()->firstName }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest

          {{--  <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-envelope"></i> 5</a></li>
            <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-bell"></i> 3</a></li>
            <li class="nav-item dropdown">
                <a href="#" id="dd_user" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Doe</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd_user">
                    <a href="#" class="dropdown-item">Profile</a>
                    <a href="#" class="dropdown-item">Logout</a>
                </div>
                </div>
            </li>--}}
        </ul>
    </div>
</nav>

<div class="d-flex">
@if(Auth::check())
    {{--CORRECXT THIS BY CUSTOM DIRECTIVE--}}
    @if(Auth::user()->role == 1)
    {{--@if(Auth::check() && Auth::user()->role == 1)--}}
       {{--Hello Admin--}}
    {{--AdminSidebar--}}
    <div class="sidebar sidebar-dark bg-dark">
       <ul class="list-unstyled">
           <li>
               <a href="#sm_expand_1" data-toggle="collapse">
                   <i class="fa fa-fw fa-link"></i>Employees section
               </a>
               <ul id="sm_expand_1" class="list-unstyled collapse">
                   <li><a href="{{ route('user.index') }}">Employees</a></li>
                   <li><a href="{{ route('user.create') }}">Add new Employee</a></li>
               </ul>
           </li>
           <li>
               <a href="#sm_expand_2" data-toggle="collapse">
                   <i class="fa fa-fw fa-link"></i>Teams section
               </a>
               <ul id="sm_expand_2" class="list-unstyled collapse">
                   <li><a href="{{ route('team.index') }}">Teams</a></li>
                   <li><a href="{{ route('team.create') }}">Add new Team</a></li>
               </ul>
           </li>
           <li><a href="{{ route('request.index') }}"><i class="fa fa-fw fa-link"></i>Requests section</a></li>
       </ul>
    </div>
    {{--End of AdminSidebar--}}
    @else
       {{--UserSidebar--}}
       <div class="sidebar sidebar-dark bg-dark">
           <ul class="list-unstyled">
               <li><a href="{{ route('myinfo.index')}}"><i class="fa fa-fw fa-link"></i>My Info</a></li>
               <li>
                   <a href="#sm_expand_2" data-toggle="collapse">
                       <i class="fa fa-fw fa-link"></i>Dashboard
                   </a>
                   <ul id="sm_expand_2" class="list-unstyled collapse">
                       <li><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                       <li><a href="{{ route('dashboard.create') }}">Create new DaysOff Request</a></li>
                       {{--<li><a href="#">Pending Requests</a></li>--}}
                       {{--<li><a href="#">History</a></li>--}}
                   </ul>
               </li>
           </ul>
       </div>
       {{--End of UserSidebar--}}
    @endif
@endif

<div class="content p-4">
   {{--<h2 class="mb-4">Blank/Starter</h2>--}}
   <div class="card mb-4">
       <div class="card-body">
           <main class="py-4">
               {{--This is a blank page you can use as a starting point.--}}
               @yield('content')
           </main>
       </div>
   </div>
</div>
</div>

<nav class="navbar navbar-expand navbar-dark bg-primary">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="navbar-brand"  href="{{ route('user.index') }}">© 2018 TimeOFF Management</a>
        </li>
    </ul>
</nav>
</body>
</html>
