@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Change Your Data:') }}</div>
                    <div class="card-body">
                        <form   action="{{ route('myinfo.update') }}" method="post" autocomplete="on">
                            <div class="form-group row">
                            <label for="firstName" class="col-xs-12 col-sm-2 col-form-label">First Name:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" name="firstName" id="firstName" value="{{ old('firstName', $user->firstName) }}" class="form-control is-valid">
                                @if($errors->has('firstName'))
                                    <label for="firstName" class="text-danger">{{ $errors->first('firstName') }}</label>
                                @endif
                            </div>
                            </div>
                            <div class="form-group row">
                            <label for="lastName" class="col-xs-12 col-sm-2 col-form-label">Last Name:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" name="lastName" id="lastName" value="{{ old('lastName', $user->lastName) }}" class="form-control is-valid">
                                @if($errors->has('lastName'))
                                    <label for="lastName" class="text-danger">{{ $errors->first('lastName') }}</label>
                                @endif
                            </div>
                            </div>
                            <div class="form-group row">
                            <label for="email" class="col-xs-12 col-sm-2 col-form-label">Email:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" name="email" id="email" value="{{ old('email', $user->email) }}" class="form-control is-valid">
                                @if($errors->has('email'))
                                    <label for="email" class="text-danger">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                            </div>

                            <div class="form-group row">
                                {{--<label class="col-md-4 col-form-label text-md-right"></label>--}}
                                <label class="col-xs-12 col-sm-2 col-form-label">{{ __('Gender') }}</label>
                                <div class="col-md-6">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons"  title="Select Your Gender">
                                        <label for="gender" class="btn btn-secondary @if($user->gender === 'male') active @endif">
                                            <input type="radio" name="gender" id="option1" autocomplete="off" value="male" @if($user->gender ===  'male') checked="checked" @endif /> Male
                                        </label>
                                        <label for="gender" class="btn btn-secondary @if($user->gender === 'female') active @endif">
                                            <input type="radio" name="gender" id="option2" autocomplete="off" value="female" @if($user->gender ===  'female') checked="checked" @endif /> Female
                                        </label>
                                    </div>

                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $errors->first('gender') }}</strong>
                                                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" value="send">
                                        {{ __('UPDATE') }}
                                    </button>
                                </div>
                            </div>
                            <input name="_method" value="PUT" type="hidden" >
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection