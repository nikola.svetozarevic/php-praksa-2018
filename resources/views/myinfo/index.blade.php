@extends('layouts.app')
@section('content')
    <table border="1">
    <tr>
        <th scope="col" class="table-primary">ID</th>
        <td>{{ $user->id }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">First Name</th>
        <td>{{ $user->firstName }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">Last Name</th>
        <td>{{ $user->lastName }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">Email</th>
        <td>{{ $user->email }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">Gender</th>
        <td>{{ $user->gender }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">Total DaysOff</th>
        <td>{{ $user->earnedDaysOff }}</td>
    </tr>
    <tr>
        <th scope="col" class="table-primary">Teams Membership</th>
        <td>
            @foreach($user->teams as $team)
                {{ $team->teamName }}
                @if (!$loop->last), @endif
            @endforeach
        </td>
    </tr>
    </table><br>
<div class="col-xs-12 col-sm-2 add_new">
<a href="{{ route('myinfo.edit') }}" class="btn btn-secondary home">Change Your Personal Data</a>
</div>
@endsection
