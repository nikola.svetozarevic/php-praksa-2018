@extends('layouts.app')
@section('content')
    <div class="card-body">
        <h4>All Pending Requests:</h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">request ID</th>
                <th scope="col">Employee name</th>
                <th scope="col">From</th>
                <th scope="col">To</th>
                <th scope="col">Requested DaysOff</th>
                <th scope="col">EARNED DaysOff</th>
                <th scope="col">Teams Membership</th>
                <th scope="col">Note</th>
                <th scope="col">Respond</th>

            </tr>
            </thead>
            <tbody>
            @php($rowOrder = "table-secondary")
            @foreach($requests as $request)
                @php($rowOrder = ($rowOrder === "table-secondary") ? 'table-primary' : 'table-secondary')
                <tr class="{{$rowOrder}}">
                    <th scope="row">{{ $request->id }}</th>
                    <td>
                        <a target="_blank" rel="noopener noreferrer" href="{{ route('user.show', ['id' => $request->user_id]) }}">{{ $request->user->firstName . ' ' . $request->user->lastName }}</a>
                    </td>
                    <td>{{ $request->from }}</></td>
                    <td>{{ $request->to }}</td>
                    <td>{{ $request->requestedDaysOff }}</td>
                    <td>{{ $request->user->earnedDaysOff}}</td>
                    <td>
                        @foreach($request->user->teams as $team)
                            <a target="_blank" rel="noopener noreferrer" href="{{ route('team.show', ['id' => $team->id]) }}">{{ $team->teamName }}</a>
                            @if (!$loop->last), @endif
                        @endforeach
                    </td>
                    <td>{{ $request->note }}</td>
                    <td>

                        {{--MODAL RESPOND BUTTON--}}
                        <div class="mb-3">
                            <button type="button" class="btn btn-primary respond-btn" data-toggle="modal" data-target="#exampleModal" data-request_id = "{{ $request->id }}" data-request_employee-name = "{{ $request->user->firstName . ' ' . $request->user->lastName }}" >RESPOND</button>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{--MODAL BODY--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form   action="{{ route('request.update')  }}" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Respond to <span id="username_span"></span> request</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <textarea id="explanation" name="explanation" class="form-control{{ $errors->has('explanation') ? ' is-invalid' : '' }}">{{ old('explanation') }}</textarea>
                    </div>
                    <div class="modal-footer">
                        {{--MODAL ACCEPT/REJECT BUTTON--}}

                        <input type="hidden" name="request_id" id="request_id">
                        {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                        <button type="submit" name="accept" value="accept" class="btn btn-primary">ACCEPT</button>
                        <button type="submit" name="reject" value="reject" class="btn btn-danger">REJECT</button>
                    </div>
                    @method('PUT')
                    @csrf
                </form>
            </div>
        </div>
    </div>
    <script>

        $( document ).ready(function() {
            $('.respond-btn').click(function () {
                $('#username_span').html($(this).attr('data-request_employee-name'));
                $('#request_id').val($(this).attr('data-request_id'));
            })
        });
    </script>


                {{--LARAVEL NEXT and PREVIOWS--}}
    {{ $requests->render() }}
@endsection



