@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create new Team') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('team.store') }}" aria-label="{{ __('Create new Team') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="teamName" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="teamName" type="text" class="form-control{{ $errors->has('teamName') ? ' is-invalid' : '' }}" name="teamName" value="{{ old('teamName') }}" required autofocus placeholder="Team Name">
                                    @if ($errors->has('teamName'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('teamName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection







