@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Change Team data:') }}</div>
                    <div class="card-body">
                        <form   action="{{ route('team.update', ['id' => $team->id]) }}" method="post">
                            <div class="form-group row">
                            <label for="teamName" class="col-xs-12 col-sm-2 col-form-label">Team Name:</label>
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" name="teamName" id="teamName" value="{{ old('teamName', $team->teamName) }}" class="form-control is-valid">
                                @if($errors->has('teamName'))
                                    <label for="teamName" class="text-danger">{{ $errors->first('teamName') }}</label>
                                @endif
                            </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" value="send">
                                        {{ __('UPDATE') }}
                                    </button>
                                </div>
                            </div>
                            <input name="_method" value="PUT" type="hidden" >
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection