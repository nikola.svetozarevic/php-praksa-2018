@extends('layouts.app')
@section('content')
    <div class="card-body">
        <h4>Teams List:</h4>
        <div class="col-xs-12 col-sm-2 add_new">
            <a href="{{ route('team.create') }}" class="btn btn-secondary">ADD NEW TEAM</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Team Name</th>
                <th scope="col">Member Count</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @php($rowOrder = "table-secondary")
            @foreach($teams as $team)
                @php($rowOrder = ($rowOrder === "table-secondary") ? 'table-primary' : 'table-secondary')
                <tr class="{{$rowOrder}}">
                    <th scope="row">{{ $team->id }}</th>
                    <td><a href="{{ route('team.show', ['id' => $team->id]) }}">{{ $team->teamName }}</a></td>
                    <td>
                        {{ $team->users_count }}
                    </td>
                    <td><a href="{{ route('team.edit', ['id' => $team->id]) }}" class="btn btn-primary">EDIT</a></td>
                    <td>
                        <form   action="{{ route('team.destroy', ['id' => $team->id])  }}" method="post">
                            <div class="col-xs-12 col-sm-4">
                                <button type="submit" value="send" class="btn btn-danger">DELETE</button>
                            </div>
                            @method('DELETE')
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
    </tbody>
</table>
</div>
{{--LARAVEL NEXT AND PREVIOWS --}}
{{ $teams->render() }}
@endsection
