@extends('layouts.app')
@section('content')
    <div class="card-body">
    <h3>Team {{ $team->teamName }} (with ID {{  $team->id  }})  containes  {{ $team->users_count }} users:</h3>
    <div class="col-xs-12 col-sm-2 add_new_from_team">
    <a href="{{ route('user.create', ['teamPreChecked' => $team->id]) }}" class="btn btn-secondary">ADD NEW EMPLOYEE TO THE {{ $team->teamName }} TEAM </a>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">FirstName</th>
            <th scope="col">LastName</th>
            <th scope="col">Email</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @php($rowOrder = "table-secondary")
        @foreach($team->users as $user)
            @php($rowOrder = ($rowOrder === "table-secondary") ? 'table-primary' : 'table-secondary')
            <tr class="{{$rowOrder}}">
                <th scope="row">{{ $user->id }}</th>
                <td><a href="{{ route('user.show', ['id' => $user->id]) }}">{{ $user->firstName }}</a></td>
                <td>{{ $user->lastName }}</td>
                <td>{{ $user->email }}</td>
                <td><a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-primary">EDIT</a></td>
                <td>
                    <form   action="{{ route('user.destroy', ['id' => $user->id])  }}" method="post">
                        <div class="col-xs-12 col-sm-4">
                            <button type="submit" value="send" class="btn btn-danger">DELETE</button>
                        </div>
                        @method('DELETE')
                        @csrf
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
