@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Create new User') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('user.store', ["teamPreChecked" => $teamPreChecked]) }}" aria-label="{{ __('Create new User') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="firstName" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="firstName" type="text" class="form-control{{ $errors->has('firstName') ? ' is-invalid' : '' }}" name="firstName" value="{{ old('firstName') }}" required autofocus placeholder="First Name">
                                    @if ($errors->has('firstName'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ old('lastName') }}" required placeholder="Last Name">

                                    @if ($errors->has('lastName'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="Password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons"  title="Select Your Gender">
                                        <label for="gender" class="btn btn-secondary">
                                            <input type="radio" name="gender" id="option1" autocomplete="off" value="male" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" required> Male
                                        </label>
                                        <label for="gender" class="btn btn-secondary">
                                            <input type="radio" name="gender" id="option2" autocomplete="off" value="female" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" required> Female
                                        </label>
                                    </div>
                                    @if ($errors->has('gender'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="teams" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                <h6>Select Team Membership</h6>
                                @foreach($teams as $team)
                                    <input type="checkbox" name="teams[]" value="{{ $team->id }}" @if ( (in_array($team->id, old('teams', $teamsChecked))) || (($team->id) == $teamPreChecked)) checked="checked" @endif>
                                        &nbsp{{ $team->teamName }} &nbsp &nbsp &nbsp
                                @endforeach
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="init-days-off" class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <input id="init-days-off" type="number" class="form-control" name="earnedDaysOff" min="0" max="20"
                                           value="{{ old('earnedDaysOff') }}" required placeholder="Initial Number of Days-Off">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection







