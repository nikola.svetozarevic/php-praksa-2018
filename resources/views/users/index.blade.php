@extends('layouts.app')
@section('content')
    {{--Search and show results in the same pagebo!--}}
    <div class="container">
        <form action="{{ route('user.index') }}" method="GET" role="search">
            <h5>Search names and emails</h5>
            <div class="input-group">
                <input type="text" class="form-control" name="search" placeholder="Search users" value="{{ $search  }}"><br>
            </div>
            <div>
                <br><h5>Filter by Team membership</h5>
                @foreach($teams as $team)
                    <input type="checkbox" name="teams[]" value="{{ $team->id }}" @if (in_array($team->id, $teamsChecked)) {{ 'checked' }} @endif>
                    {{ $team->teamName }}<br>
                @endforeach

                {{--DATEPICKER jQuery--}}
                <br><h5>Filter by Period - Select a Date Range</h5>
                <label for="from">From</label>
                <input type="text" id="from" name="fromPicked" value="{{ $fromPicked  }}">
                <label for="to">to</label>
                <input type="text" id="to" name="toPicked" value="{{ $toPicked  }}">
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" value="submit" class="btn btn-primary">
                        {{ __('Search') }}
                    </button>
                </div>
            </div>
            {{--</div>--}}
        </form>
    </div>

    <div class="card-body">
        <h4>Employees List:</h4>
        <div class="col-xs-12 col-sm-2 add_new">
        {{--<div class="col-xs-12 col-sm-4">--}}
            <a href="{{ route('user.create') }}" class="btn btn-secondary">ADD NEW EMPLOYEE</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Employee</th>
                <th scope="col">Email</th>
                <th scope="col">Teams Membership</th>
                <th scope="col">Approved Holidays (ahead)</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
    {{--<a href="{{ route('user.show', ['id' => $user->id]) }}">{{ $user->name }}</a>--}}
            <tbody>
            @php($rowOrder = "table-secondary")
            @foreach($users as $user)
                @php($rowOrder = ($rowOrder === "table-secondary") ? 'table-primary' : 'table-secondary')
                <tr class="{{$rowOrder}}">
                    <th scope="row">{{ $user->id }}</th>
                    <td>
                        <a href="{{ route('user.show', ['id' => $user->id]) }}">{{ $user->firstName . ' ' . $user->lastName }}</a>
                    </td>
                    <td>{{ $user->email }}</td>
                    <td>
                        {{--@if($user->teams()->count() > 1)--}}
                        @foreach($user->teams as $team)
                            {{ $team->teamName }}
                            @if (!$loop->last), @endif
                        @endforeach
                        {{--@else--}}
                        {{--Ima manje od dozvoljenog: {{ $user->teams->count() }}--}}
                        {{--@endif--}}
                    </td>
                    <td>
                        @foreach($user->requests as $request)
                            {{--$from = Carbon::createFromFormat('Y-m-d', $request->$from);--}}
                            {{ $request->getDateRange() }}
                            @if (!$loop->last), @endif
                        @endforeach
                    </td>
                    <td>
                        <a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-primary">EDIT</a>
                    </td>
                    <td>
                    <form   action="{{ route('user.destroy', ['id' => $user->id])  }}" method="post">
                        <div class="col-xs-12 col-sm-4">
                            <button type="submit" value="send" class="btn btn-danger">DELETE</button>
                        </div>
                        @method('DELETE')
                        @csrf
                    </form>
                    </td>
                </tr>
            @endforeach
    </tbody>
</table>
</div>

{{--LARAVEL NEXT I PREVIOWS --}}
{{ $users->render() }}
{{--MANUELNA NEXT I PREVIOWS --}}
{{--@if ($page != 0)
<a href="{{ route('user.index', ['page' => $page - 1]) }}"><button class="btn btn-primary" value="previous">Previous</button></a>
@endif
<a href="{{ route('user.index', ['page' => $page + 1]) }}"><button class="btn btn-primary" value="next">Next</button></a>--}}

@endsection
