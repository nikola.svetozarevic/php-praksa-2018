@extends('layouts.app')
@section('content')

<table border="1">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Email</th>
        <th scope="col">Gender</th>
        <th scope="col">Total DaysOff</th>
        <th scope="col">Teams Membership</th>
        <th scope="col">Role</th>
    </tr>
    <tr class="table-primary">
        <td>{{ $user->id }}</td>
        <td>{{ $user->firstName }}</td>
        <td>{{ $user->lastName }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->gender }}</td>
        <td>{{ $user->earnedDaysOff }}</td>
        <td>
            @foreach($user->teams as $team)
                {{ $team->teamName }}
                @if (!$loop->last), @endif
            @endforeach
        </td>
        <td>{{ $user->role }}</td>
    </tr>
</table>
</br>
<div class="col-xs-12 col-sm-2 add_new">
<a href="{{ url('/user') }}" class="btn btn-secondary home">Employee List</a>
</div>
@endsection
