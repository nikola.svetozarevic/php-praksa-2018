<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\User;
use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin routes

Route::middleware(['admin'])->group(function () {
    Route::resource('/user', 'Admin\UserController');
    Route::resource('/team', 'Admin\TeamController');
    Route::get('/request', 'Admin\RequestController@index')->name('request.index');
    Route::put('/request', 'Admin\RequestController@update')->name('request.update');
});

//Employee routes

Route::middleware(['employee'])->group(function () {
    Route::get('/myinfo', 'Employee\MyInfoController@index')->name('myinfo.index');
    Route::get('/myinfo/edit', 'Employee\MyInfoController@edit')->name('myinfo.edit');
    Route::put('/myinfo/update', 'Employee\MyInfoController@update')->name('myinfo.update');

    Route::get('/dashboard', 'Employee\DashboardController@index')->name('dashboard.index');
    Route::get('/dashboard/create', 'Employee\DashboardController@create')->name('dashboard.create');
    Route::post('/dashboard', 'Employee\DashboardController@store')->name('dashboard.store');
});
